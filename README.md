# Práctica: Fumadores

## Enunciado

Considere un sistema formado por tres __fumadores__ que se pasan el día liando cigarros y fumando. Para liar un cigarro se necesitan 3 ingredientes: tabaco, papel y cerilla. Cada fumador dispone de un surtido suficiente (para el resto de su vida) de uno de los tres ingredientes, es decir, cada fumador tiene un único ingrediente:

* _fumador1_: tiene una cantidad infinita de tabaco
* _fumador2_: tiene una cantidad infinita de papel
* _fumador3_: tiene una cantidad infinita de cerillas

Hay también un __agente__ que pone dos de los tres ingredientes encima de la mesa, es decir, si el fumador1 tiene una cantidad infinita de tabajo, el agente pondrá sobre mesa papel y cerillas, que son los ingredientes que le faltan. El "agente" dispone de una reservas infinitas de cada uno de los tres ingredientes, escogiendo de forma aleatoria cuáles son los dos ingredientes que pondrá sobre la mesa. Cuando los ha puesto, el fumador que tiene el otro ingrediente puede fumar (los otros dos no). Para ello, coge los tres ingredientes, se lía un cigarro y se lo fuma. Cuando termina de fumar vuelve a repetirse el ciclo.

En resumen, el ciclo que debe repetirse es:

 * _agente_ pone ingredientes encima de la mesas.
 * _fumador_ lía un cigarro.
 * _fumador_ fuma el cigarro.
 * _fumador_ termina de fumar.

> Nota: el modelado de poner dos ingredientes encima de la mesa es más sencillo si se indica cuál de los ingredientes no se pone: por ejemplo, si queremos poner tabaco y papel, indicamos cerillas.

Para este problema se deben crear 4 clases: Agente, Fumador, Mesa y Main.

A continuación, se muestra un esqueleto de la clase Mesa:

```java
public class Mesa {

	// 0-tabaco, 1-papel, 2-cerillas

	// El agente pone los dos ingredientes distintos a ingr en la mesa
	// Comentario para e agente: El agente no puede poner los ingredientes
	// en la mesa hasta que el fumador haya terminado de fumar

	public synchronized void nuevosIngredientes(int ingr) throws InterruptedException {

	}

	// El fumador id quiere fumar
	// Comentario para los fumadores: el fumador no puede fumar hasta que los
	// ingredientes que le fantal no estén sobre la mesa

	public synchronized void quieroFumar(int id) throws InterruptedException {

	}

	// El fumador id ha terminado de fumar

	public synchronized void finFumar(int id) {

	}

}
```
